var readBooksPromise = require('./promise.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 },
]

function bookStart(key, time) {
    if (key > books.length - 1) return '';
   readBooksPromise(time, books[key], time => {
        return time;
    }).then((data) => {
        return key + bookStart(key + 1, data);
    }).catch((error) => {
        return error;
    });
}

bookStart(0, 10000);