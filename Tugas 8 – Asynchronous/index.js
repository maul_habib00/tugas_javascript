var readBooks = require('./callback.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 },
];

function bookStart(key, time){
    if (key > books.length -1) return '';
    readBooks(time, books[key], time => {
        return key + bookStart(key+1, time);   
    })
}

bookStart(0, 10000);