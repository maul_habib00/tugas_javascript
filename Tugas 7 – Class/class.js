// Tugas 7 – Class
// case 1
// release 0
console.log('Soal 1');
class Animal {
    // Code class di sini
    constructor(name, legs, cold_blooded){
        this.name = name;
        this._legas = legs;
        this._cold_blooded = cold_blooded;
    }
    get legs() {
        return this._legs;
    }
    set legs(x) {
        this._legs = x;
    }
    get cold_blooded() {
        return this._cold_blooded;
    }
    set cold_blooded(x) {
        this._cold_blooded = x;
    }
}

var sheep = new Animal("shaun");
sheep.legs = 4;
sheep.cold_blooded = false;
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

// release 1
// Code class Ape dan class Frog di sini
class Ape extends Animal {
    constructor(name) {
        super(name);
    }
    yell(){
        console.log('Auooo');
    }
}
class Frog extends Animal {
    constructor(name) {
        super(name);
    }
    jump(){
        console.log('hop hop');
    }
}

const sungokong = new Ape("kera sakti");
sungokong.legs = 2;
sungokong.cold_blooded = false;
console.log(sungokong.name); // "kera sakti"
console.log(sungokong.legs); // 2
console.log(sungokong.cold_blooded); // false
sungokong.yell() // "Auooo"

const kodok = new Frog("buduk");
kodok.legs = 4;
kodok.cold_blooded = true;
console.log(kodok.name); // "buduk"
console.log(kodok.legs); // 4
console.log(kodok.cold_blooded); // true
kodok.jump() // "hop hop" 
// case 2
console.log('Soal 2');

class Clock {
    // Code di sini
  constructor(template, timer){
        this.template = template;
        this.timer = timer;
    }
  
  start() {
        this.render();
        this.stop();
        this.timer = setInterval(() => {
            this.render();
        }, 1000);
    }
  
  stop() {
        clearInterval(this.timer);
    }
  
  render(){
        let template = this.template.template;
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);

        console.log(output);
    }
}

var clock = new Clock({ template: 'h:m:s' });
clock.start();