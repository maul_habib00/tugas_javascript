// Tugas 5 – Array
// case 1 - Range
// Code di sini
console.log('Soal No. 1 (Range)')
function range(numstart, numend){
    if(!numstart || !numend) return -1;
    let array = [];
    let numeStart, numeEnd;
    numeStart = numstart > numend ? numend : numstart;
    numeEnd = numstart > numend ? numstart : numend;
    for (let index = numeStart; index <= numeEnd;) {
        array.push(index)
        index++;
    }
    if (numstart > numend) {
        array.reverse();
    }
    return array;
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 
// case 2 - Range with Step
// Code di sini
console.log('Soal No. 2 (Range with Step)')
function rangeWithStep(numstart, numend, step) {
  if (!numstart || !numend) return -1;
    let array = [];
    if (numstart > numend) {
        for (let index = numstart; index >= numend; index-=step) {
            array.push(index)
        }
    }else{
        for (let index = numstart; index <= numend; index+=step) {
            array.push(index)
        }
    }
    return array;

}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
// case 3 - Sum of Range
// Code di sini
console.log('Soal No. 3 (Sum of Range)')
function sum(numstart = 0, numend = 0, step = 1){
  let array = [];
    let jum = 0;
    if (numstart > numend) {
        for (let index = numstart; index >= numend; index -= step) {
            array.push(index)
            jum+=index;
        }
    } else {
        for (let index = numstart; index <= numend; index += step) {
            array.push(index)
            jum += index;
        }
    }
    return jum;

}
console.log(sum(1, 10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 
// case 4 - Array Multidimensi
// Code di sini
console.log('Soal No. 4 (Array Multidimensi)')
//contoh input
function dataHandling(data){
 for (let isi = 0; isi < data.length; isi++){
   console.log(`\nNomor ID: ${data[isi][0]}`)
   console.log(`Nama: ${data[isi][1]}`)
   console.log(`TTL: ${data[isi][2]} ${data[isi][3]}`)
   console.log(`Hobi: ${data[isi][4]}`)
 } 
}
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
console.log(dataHandling(input));
// output
// Nomor ID: 0001
// Nama Lengkap: Roman Alamsyah
// TTL: Bandar Lampung 21 / 05 / 1989
// Hobi: Membaca

// Nomor ID: 0002
// Nama Lengkap: Dika Sembiring
// TTL: Medan 10 / 10 / 1992
// Hobi: Bermain Gitar

// Nomor ID: 0003
// Nama Lengkap: Winona
// TTL: Ambon 25 / 12 / 1965
// Hobi: Memasak

// Nomor ID: 0004
// Nama Lengkap: Bintang Senjaya
// TTL: Martapura 6 / 4 / 1970
// Hobi: Berkebun 
// case 5 - Balik kata
// Code di sini
console.log('No. 5 (Balik Kata)')
// Code di sini
function balikKata(text){
  let result = '';
  let kata = text.length-1;
  for (let index = kata; index >= 0; index--){
    result += text[index];
  }
  return result;
}

console.log(balikKata("Kasur Rusak"))// kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 
// case 6 - Metode Array
// Code di sini
console.log('No. 6 (Metode Array)') // terserah mau buat function seberapa banyak
function datatoString(data) {
    for (let index = 0; index < data.length; index++) {
        data[index] = String(data[index]);
    }
    return data;
}
function datatoNumber(data) {
    for (let index = 0; index < data.length; index++) {
        data[index] = Number(data[index]);
    }
    return data;
}
function gantiTanggal(date){
    let data = date.split('/');
    let bulan = Number(data[1]);
    switch (bulan) {
        case 1: { bulan = 'Januari'; break; }
        case 2: { bulan = 'Februari'; break; }
        case 3: { bulan = 'Maret'; break; }
        case 4: { bulan = 'April'; break; }
        case 5: { bulan = 'Mei'; break; }
        case 6: { bulan = 'Juni'; break; }
        case 7: { bulan = 'Juli'; break; }
        case 8: { bulan = 'Agustus'; break; }
        case 9: { bulan = 'September'; break; }
        case 10: { bulan = 'Oktober'; break; }
        case 11: { bulan = 'November'; break; }
        case 12: { bulan = 'Desember'; break; }
        default: { console.log('Tidak terjadi apa-apa'); }
    }
    let text = `${data[0]}-${bulan}-${data[2]}`;
    console.log(bulan);
    data = datatoNumber(data);
    let sortDesc = data.sort();
    data = datatoString(data);
    console.log(data);
    return text;
}
function pembatanString(string, batas = 1){
    let text = '';
    batas -= 1;
    for (let index = 0; index < string.length; index++) {
        if(index <= batas){
            text += string[index];
        }
    }
    return text;
}
function dataHandling2(dataarray){
    let data = dataarray[0];
    //contoh output
    // ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]  
    data[1] = 'Roman Alamsyah Elsharawy';
    data[2] = 'Provinsi Bandar Lampung';
    data.splice(4, 4, 'Pria');
    data.splice(5, 5, 'SMA Internasional Metro');
    console.log(data);
    // ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"] 
    const tanggal = gantiTanggal(data[3]);
    let split = data[3].split('/');
    let joinData = split.join("-");
    console.log(joinData);
    console.log(pembatanString(data[1], 15));
}
dataHandling2(input);

/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */