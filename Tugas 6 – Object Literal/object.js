// Tugas 6 – Object Literal
// case 1
console.log('Soal 1');
function arrayToObject(arr) {
    // Code di sini 
    var now = new Date()
    var thisYear = now.getFullYear()
    let buatText = '';
    if(arr.length > 0) {
        arr.forEach((array, index) => {
            let buatObject = {};
            buatObject.firstName = array[0];s
            buatObject.lastName = array[1];
            buatObject.gender = array[2];
            buatObject.age = array[3] ? array[3] : 'Invalid Birth Year';
            buatText += `${index+1}. ${buatObject.firstName} ${buatObject.lastName}: ${JSON.stringify(buatObject)} \n`;
        });
    }else{
        buatText = '""';
    }
    console.log(buatText);
}


// Driver Code
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people)
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2)
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case 
arrayToObject([]) // ""

// case 2
console.log('soal 2');
const item = [
    {
        nama: 'Sepatu Stacattu',
        harga: 1500000
    },
    {
        nama: 'Baju Zoro',
        harga: 500000
    },
    {
        nama: 'Baju H&N',
        harga: 250000
    },
    {
        nama: 'Sweater Uniklooh',
        harga: 175000
    },
    {
        nama: 'Casing Handphone',
        harga: 50000
    },
];
function searchData(uang) {
    let data = [];
    let money = uang;
    let result = {};
    item.forEach(element => {
        const kurangin = money - element.harga;
        let checkKecukupan = kurangin >= 0 ? true : false;
        if (element.harga <= money && checkKecukupan) {
            data.push(element.nama);
            money -= element.harga;
        }
    });
    result = {
        data: data,
        sisa: money,
    };
    return result;
}
function shoppingTime(memberId, money) {
  // you can only write your code here!
  const objectData = {};
  if(!memberId) return "Mohon maaf, toko x hanya berlaku untuk member saja";
  objectData.memberId = memberId;
  objectData.money = money;
  const findData = searchData (money);
  objectData.listPurchased = findData.data;
  if (objectData.listPurchased.length < 1) return "Mohon maaf, uang anda tidak cukup";
  objectData.changeMoney = findData.sisa;
  return objectData;
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// case 3
console.log('Soal 3')
const rute = ['A', 'B', 'C', 'D', 'E', 'F'];
// per rute bayar 2000
function checkBayar(dari, sampe){
  let berangkat = rute.indexOf(dari);
  let tujuan = rute.indexOf(sampe);
  const harga = (tujuan - berangkat)*2000;
  return harga;
}
function naikAngkot(arrPenumpang) {
    //your code here
  let data = [];
  if (arrPenumpang.length < 1) return [];
  arrPenumpang.forEach(element => {
    let buatData = {};
    buatData.penumpang = element[0];
    buatData.naikDari = element[1];
    buatData.tujuan = element[2];
    buatData.bayar = checkBayar(element[1], element[2]);
    data.push(buatData);
  });
  return data;
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]